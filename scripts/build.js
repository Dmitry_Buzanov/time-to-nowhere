'use strict';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
require('./../config');

let nodeVersion = parseFloat(process.version.substring(1));

if (nodeVersion >= 7.6) {
  require('./../src/app');
} else {
  let register = require('babel-core/register');

  register({
    presets: ['stage-3']
  });

  require('./../src/app');
}
