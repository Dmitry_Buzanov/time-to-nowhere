const passport = require('koa-passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const User = require('../../db/models/Users/index');
const mongoose = require("mongoose");

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    async (email, password, done) => {

        User.findOne({email}, async (err, user) => {
            if (err) return done(err);
            if (!user) return done(null, false, {message: 'User not found'});
            const isPasswordValid = await user.comparePasswords(password);
            if (!isPasswordValid) return done(null, false, {message: 'Invalid password'});

            return done(null, user, {message: 'Logged In Successfully'});
        })
    }
));

passport.use(new FacebookStrategy({
        clientID: '329501527697849',
        clientSecret: '895c3ff8e00ce9045a50a2ff6ebcd0ab',
        callbackURL: "http://localhost:9002/auth/facebook/callback"
    },
    (accessToken, refreshToken, profile, cb) => {
    console.log(profile);
        User.findOrCreate({ facebookId: profile.id }, function (err, user) {
            return cb(err, user);
        });
    }
));

passport.use(new GoogleStrategy({
        clientID: "157248770896-cnu3j2m6p07bb1lncjll3o8t4k69g1a8.apps.googleusercontent.com",
        clientSecret: "mOV54h_nkpFL8HIKzn74dAPx",
        callbackURL: "http://localhost:9002/auth/google/callback"
    },
    async (accessToken, refreshToken, profile, done) => {

        User.findOne({ googleId: profile.id }, async (err, user) => {
            if (err) return done(err);
            console.log("emails.read",profile.email)
            if (!user) {
                user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    name: profile.displayName,
                    email: profile.displayName,
                    password: "12345678",
                    googleId: profile.id,
                });
                user.save(function(err) {
                    if (err) console.log(err);
                    return done(err, user);
                });
            }
            else {
                return done(err, user);
            }
        })
    }

));

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});


module.exports = passport;