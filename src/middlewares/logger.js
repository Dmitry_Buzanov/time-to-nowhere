let log = require('../logger').access;
function logger() {
  return async function logger(ctx, next) {
    let start = Date.now();
    await next();
    let delta = Math.ceil(Date.now() - start);
    log.info('%s - "%s" %s %sms', ctx.req.method, ctx.req.url, ctx.res.statusCode, delta);
  };
}

module.exports = logger;
