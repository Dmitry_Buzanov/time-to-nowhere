const Koa = require('koa');
const router = require('koa-router')();
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const json = require('koa-json');
const signupUser = require('../controllers/signupController');
const sendForgotMail = require('../controllers/forgotController');
const sendConfirmMail = require('../controllers/confirmController');
const refreshToken = require('../controllers/refreshTokenController');
const resetPassword = require('../controllers/resetPasswordController');
const getUsers = require('../controllers/new/getUsers');
const loginUser = require('../controllers/new/loginUser');
const loginUserGoogle = require('../controllers/new/loginUserGoogle');
const loginUserGoogleCallback = require('../controllers/new/loginUserGoogleCallback');
const loginUserFacebook = require('../controllers/new/facebook/loginUserFacebook');
const loginUserFacebookCallback = require('../controllers/new/facebook/loginUserFacebookCallback');
const app = new Koa();

router.post('/signup', signupUser);
router.post('/signin', loginUser);
router.put('/forgot', sendForgotMail);
router.get('/getusers', getUsers);
router.put('/resetPassword', resetPassword);
router.get('/refresh', refreshToken);
router.post('/confirmEmail', sendConfirmMail);
router.get('/google', loginUserGoogle);
router.get('/google/callback', loginUserGoogleCallback);
router.get('/facebook', loginUserFacebook);
router.get('/facebook/callback', loginUserFacebookCallback);

app
  .use(bodyparser())
  .use(json())
  .use(router.routes(), router.allowedMethods());

module.exports = app;
