const mongoose = require('mongoose');
const User = require('../../../../db/models/Users/index');
const mailService = require('../../../services/mailService/mailService');
const config = require('../../../../config/development').links;

const signupUser = async ctx => {
  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    firstName: ctx.request.body.firstName,
    lastName: ctx.request.body.lastName,
    email: ctx.request.body.email,
    password: ctx.request.body.password
  });

  try {
    await user.save();
    /*await mailService(user2.email, 'Confirm email',confirmEmailTemplatePath, confirmUrl);*/
    ctx.status = 200;
    ctx.body = { success: true, message: 'User was registered', isConfirmed: false };
  } catch (err) {
    console.log(err.message);
    if (err.code === 11000) {
      ctx.status = 409;
      ctx.body = { success: false, message: 'This email already exist!' };
    } else {
      ctx.status = 400;
      ctx.body = { success: false, code: err.code, message: 'Uncorrect data' };
    }
  }
};

module.exports = signupUser;
