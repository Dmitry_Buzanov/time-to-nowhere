const User = require('../../../../db/models/Users/index');
const mailService = require('../../../services/mailService/mailService');
const config = require('../../../../config/development').links;

const sendForgotMail = async(ctx)=>{
    try{
        const {email} = ctx.request.body;
        const user = await User.findOne({email});
        
        if(!user){
            const err = new Error();
            err.status = 400;
            err.message = "Email not found";
            throw err;
        }

        /*await mailService(email, 'Change password', changePasswordTemplatePath, resetPasswordUrl);*/

        ctx.status = 200;
        ctx.body = {
            success: true,
            message: "Confirm your actions on the mail"
        }
    }
    catch(err) {
        if(err.status !== undefined){
            ctx.status = err.status;
            ctx.body = {success: false, message: err.message};
        }
        else{
            ctx.status = 500;
            ctx.body = {success: false, message: "Internal server error"};
        }
    }
}

module.exports = sendForgotMail;