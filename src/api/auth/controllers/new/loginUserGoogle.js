const passport = require('koa-passport');



const {generateAccessToken,generateRefreshToken,expiresIn} = require("../generateToken");

const loginUserGoogle = async (ctx, next) => {
    try{
        await passport.authenticate('google', {session: false, scope: ['profile'] }, async (err, user, info) => {
            if (err || !user) {
                const err = new Error();
                err.status = 400;
                err.message = info.message;
                throw err;
            }})(ctx);}
    catch (err) {
        if(err.status !== undefined){

            ctx.status = err.status;
            ctx.body = {success: false, message: err.message};
        }
        else{
        }
    }
};


module.exports= loginUserGoogle;