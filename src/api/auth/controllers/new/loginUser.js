const passport = require('koa-passport');

const { generateAccessToken, generateRefreshToken, expiresIn } = require('../generateToken');

const loginUser = async (ctx, next) => {
  try {
    await passport.authenticate('local', { session: false }, async (err, user, info) => {
      if (err || !user) {
        const err = new Error();
        err.status = 400;
        err.message = info.message;
        throw err;
      }
      ctx.login(user, async err => {
        if (err) {
          ctx.body = `Exeption ${err} User ${user}`;
        } else {
          const accessToken = await generateAccessToken({ _id: user._id }); // TODO: передаваемые данные в access token
          const refreshToken = await generateRefreshToken({ _id: user._id }); //TODO: структура refresh token-а

          ctx.status = 200;
          ctx.body = { success: true, accessToken, refreshToken, expiresIn };
        }
      });
    })(ctx);
  } catch (err) {
    if (err.status !== undefined) {
      ctx.status = err.status;
      ctx.body = { success: false, message: err.message };
    } else {
      console.log(err);
    }
  }
};

module.exports = loginUser;
