const jwt = require('jsonwebtoken');
const config = require('../../../../config/development').secrets;
const expiresIn = config.expires_in

const generateToken = async(...params) => {
    try{
       /* console.log("config.server.port", config.access_secret, config.refresh_secret, config.expires_in);
        console.log("config", accessSecret, refreshSecret, expiresIn);*/
        const signedData = await jwt.sign(...params);
        return signedData;
    }
    catch(err){
        err.status = 400;
        err.message = "Can't generate token";
        throw err;
    }
}

const decodeToken = async(token,key)=>{
    try{
        const decodedData = await jwt.verify(token,key);
        return decodedData;
    }
    catch(err){
        err.status = 401;
        err.message = "Uncorrect token";
        throw err;
    }
}

const generateAccessToken = async(data) =>{
    try{
        const accessToken = await generateToken(data,config.access_secret,{expiresIn});
        return accessToken;
    }
    catch(err){
        throw err;
    }
}

const generateRefreshToken = async(data) =>{
    try{
        const refreshToken = await generateToken(data,config.refresh_secret);
        return refreshToken;
    }
    catch(err){
        throw err;
    }
}

const decodeAccessToken = async(data)=>{
    try{
        const decodeData = await decodeToken(data,config.access_secret);
        return decodeData;
    }
    catch(err){
        throw err;
    }
}

const decodeRefreshToken = async(data)=>{
    try{
        const decodeData = await decodeToken(data,config.refresh_secret);
        return decodeData;
    }
    catch(err){
        throw err;
    }
}

module.exports = {generateAccessToken,generateRefreshToken,decodeAccessToken,decodeRefreshToken,expiresIn};