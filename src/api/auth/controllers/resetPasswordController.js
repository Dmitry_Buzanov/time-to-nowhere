const User = require("../../../../db/models/Users/index");
const {decodeRefreshToken} = require("./generateToken");

const resetPassword = async(ctx) =>{
    try{
        const {token, newPassword} = ctx.request.body;
        const {email} = await decodeRefreshToken(token);
        const user = await User.findOne({email});

        if(!user){
            const err = new Error();
            err.status = 400;
            err.message = "User not found";
            throw err;
        }
        user.password = newPassword;
        await user.save();
        ctx.status = 200;
        ctx.body = {success:true,message: "Password was successfully changed"};
    }
    catch(err){
        if(err.status !== undefined){
            ctx.status = err.status;
            ctx.body = {success: false, message: err.message};
        }
       else{
           console.log(err);
       }
    }
}

module.exports = resetPassword;