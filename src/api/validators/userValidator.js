const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);
Joi.objectId = require('joi-objectid')(Joi);

const userSchema = Joi.object().keys(
    {
        _id: Joi.any(),
        firstName: Joi.string()
            .alphanum()
            .min(2)
            .max(30),
        lastName: Joi.string()
            .alphanum()
            .min(2)
            .max(30),
        email: Joi.string()
            .email({minDomainAtoms: 2}),
        password: Joi.string()
            .regex(/^[a-zA-Z0-9]{3,30}$/),
        avatar: Joi.binary()
            .encoding('base64'),
        position: Joi.string(),
        birthDate: Joi.date().format('YYYY-MM-DD'),
        gender: Joi.string(),
        role: Joi.string(),
        googleId: Joi.string(),
        facebookId: Joi.string()
    });

const validateArtwork = async data => {
    try {
        return await Joi.validate(data, userSchema, {abortEarly: false});
    } catch (err) {
        return err
    }

};

module.exports = validateArtwork;
