const Joi = require('joi');

const sphereSchema = Joi.object().keys(
    {
        _id: Joi.any(),
        title: Joi.string()
            .alphanum()
            .min(2),
        category: Joi.string()
            .alphanum()
            .min(2),
        image: Joi.binary()
            .encoding('base64'),
        mentors: Joi.array(),
        students: Joi.array().items(Joi.object({
            student_id: Joi.any(),
            mentor_id: Joi.any()
        }))
    });

const validateSphere = async data => {
    return await Joi.validate(data, sphereSchema, {abortEarly: false});
};

module.exports = validateSphere;
