const Joi = require('joi');

const artworkSchema = Joi.object().keys({
  name: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  statusId: Joi.number()
    .integer()
    .required(),
  advertiserPath: Joi.string().required(),
  type: Joi.string().required(),
  dimensionsWidth: Joi.number()
    .integer()
    .required(),
  dimensionsHeight: Joi.number()
    .integer()
    .integer()
    .required(),
  modifiedDate: Joi.date().required(),
  deadline: Joi.date().required(),
  channelId: Joi.number()
    .integer()
    .required(),
  fileId: Joi.number()
    .integer()
    .required(),
  archived: Joi.boolean().required()
});

const validateArtwork = async data => {
  const result = await Joi.validate(data, artworkSchema, { abortEarly: false });
  return result;
};

module.exports = validateArtwork;
