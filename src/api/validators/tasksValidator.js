const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const taskSchema = Joi.object().keys({
    title: Joi.string().min(3).max(500),
    description: Joi.string(),
    sphere: Joi.objectId(),
    creator: Joi.objectId(),
    performer: Joi.objectId(),
    startDate: Joi.date(),
    deadLine: Joi.date(),
    completed: Joi.boolean(),
    subTasks: Joi.array().items(Joi.object().keys({
        text: Joi.string(),
        completed: Joi.boolean()
    }))
});

const validateTasks = async data => {
    const result = await Joi.validate(data, taskSchema, { abortEarly: false });
    return result;
};

module.exports = validateTasks;