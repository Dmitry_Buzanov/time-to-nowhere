const {
    getUsers,
    getUser,
    addUser,
    updateUser,
    deleteUser,
    getUserStudents
} = require('./../../../services/UserService/index');


module.exports = function (router) {
    router.get('/users/list', async (ctx, next) => {
        try {
            const users = await getUsers();
            ctx.status = 200;
            ctx.body = users;
            await next();
        } catch (err) {
             throw err
        }
    });

    router.get('/user/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const user = await getUser(id);
            ctx.status = 200;
            ctx.body = user;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.post('/user', async (ctx, next) => {
        try {
            const user = await addUser(ctx.request.body);
            ctx.status = 200;
            ctx.body = user;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.put('/user/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const isUserUpdate = await updateUser(id, ctx.request.body);
            ctx.message = isUserUpdate;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.delete('/user/:id', async (ctx, next) => {
        try {
            await deleteUser(ctx.params.id);
            ctx.status = 200;
            ctx.message = 'user deleted';
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/user/:id/students', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const user = await getUserStudents(id);
            ctx.status = 200;
            ctx.body = user;
            await next();
        } catch (err) {
            throw err
        }
    });

    return router;
};
