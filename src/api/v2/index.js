let Koa = require('Koa');
let api = new Koa();
let cors = require('koa2-cors');
let bodyparser = require('koa-bodyparser');
let log = require('./../../logger').app;
let checkit = require('checkit');
let mount = require('./../helpers/mount');
let users = require('./users/index');
const tasks = require('./tasks/index');
const router = require('koa-router')();
let spheres = require('./spheres/index');
tasks(router);
users(router);
const Router = spheres(router);

// error handler
api.use(async (ctx, next) => {
  try {
    await next();
    if (ctx.isExport) {
      return;
    }
    if (!ctx.body || typeof ctx.body === 'string') {
      ctx.body = {
        error: ctx.body || ctx.message
      };
    }
  } catch (error) {
    ctx.status = error.status || 500;
    ctx.body = {
      error: ctx.status === 500 ? 'Internal Server Error' : error.message
    };

    log.error('API error', new Date(), error);

    let env = api.env || '';
    let isDev = env.indexOf('development') === 0;
    let isTest = env.indexOf('testing') === 0;

    if (isDev) {
      ctx.body.exception = {
        type: error.name,
        message: error.message,
        dump: error
      };
    }
    if (isTest) {
      ctx.body.exception = {
        test: 'test'
      };
    }
    ctx.app.emit('error', error, ctx);
  }
});

// validation error handler
api.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    if (error instanceof checkit.Error || error.name === 'Error') {
      ctx.response.status = 400;
      ctx.body = {
        error: error.message,
        data: error.toJSON()
      };
    } else {
      throw error;
    }
  }
});
api
  .use(cors())
  .use(bodyparser({ strict: false, jsonLimit: '20mb' }))
  .use(Router.routes(), Router.allowedMethods());

module.exports = api;
