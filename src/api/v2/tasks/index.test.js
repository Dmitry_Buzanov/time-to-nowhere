const app = require('../../../app');
const request = require('supertest');

beforeAll(async () => {
  async function clearDB() {
    await Promise.all(
      Object.keys(mongoose.connection.collections).map(async key => {
        return await mongoose.connection.collections[key].remove({});
      })
    );
  }

  if (mongoose.connection.readyState === 0) {
    await mongoose.connect(
      config.db.connection.url,
      { useNewUrlParser: true }
    );
  }
  await clearDB();
});

afterAll(async () => {
  await mongoose.connection.close();
});

describe('GET api/v2/tasks', () => {
  test('/tasks/list should return tasks', async () => {
    const response = await request(app.callback()).get('api/v2/tasks/list');
  });
});

describe('GET api/v2/task', () => {
  test('/task/:id should return task', async () => {
    const response = await request(app.callback()).get('api/v2/task/:id');
  });
});

describe('GET api/v2/task', () => {
  test('/tasks/user/:id/list should return tasks list by user id', async () => {
    const response = await request(app.callback()).get('api/v2/tasks/user/:id/list');
  });
});

describe('GET api/v2/tasks', () => {
  test('/tasks/sphere/:id/list should return tasks', async () => {
    const response = await request(app.callback()).get('api/v2/tasks/sphere/:id/list');
  });
});

describe('POST api/v2/task', () => {
  test('/task should create a task', async () => {
    const response = await request(app.callback()).get('api/v2/task');
  });
});

describe('PUT api/v2/task', () => {
  test('/task/:id should update task', async () => {
    const response = await request(app.callback()).get('api/v2/task/:id');
  });
});

describe('DELETE api/v2/tasks', () => {
  test('/task/:id should delete tasks', async () => {
    const response = await request(app.callback()).get('api/v2/task/:id');
  });
});
