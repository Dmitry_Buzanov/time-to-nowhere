

const {
    getTasks,
    getTasksById,
    addTask,
    updateTask,
    deleteTask
} = require('./../../../services/TasksService/index');

module.exports = function (router) {

    router.get('/tasks/list', async (ctx, next) => {
        try {
            const tasks = await getTasks();
            ctx.status = 200;
            ctx.body = { tasks: tasks, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/task/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = '_id';
            const task = await getTasksById(id, matchingKey);
            ctx.status = 200;
            ctx.body = { tasks: task, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/tasks/creator/:id/list', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = ctx.url.split('/')[2];
            const tasks = await getTasksById(id, matchingKey);
            ctx.status = 200;
            ctx.body = { tasks: tasks, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/tasks/performer/:id/list', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = ctx.url.split('/')[2];
            console.log(matchingKey)
            const tasks = await getTasksById(id, matchingKey);
            ctx.status = 200;
            ctx.body = { tasks: tasks, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/tasks/performer/:id/tabletasks', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = ctx.url.split('/')[2];
            console.log(matchingKey)
            const tasks = await getTasksById(id, matchingKey);
            const data = {
                tasksTitles: ["Name of the Sphere", "Name of the task", "Creator"],
                tasksRows: tasks.map((task, index) => ({
                            name_of_the_sphere: task.sphere.title,
                            name_of_the_task: task.title,
                            creator: task.creator.firstName + " " + task.creator.lastName,
                            id: task._id.toString(),
                            isSelected: false,
                            img: "http://xochu-vse-znat.ru/wp-content/uploads/2017/11/vbyb.jpg"
                }))
            }
            ctx.status = 200;
            ctx.body = { tasks: data, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/tasks/performer/:id/tablemain', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = ctx.url.split('/')[2];
            console.log(matchingKey)
            const tasks = await getTasksById(id, matchingKey);

            const data = {
                tasksTitles: ["Task", "Sphere", "Mentor", "Start date", "Deadline", "Status"],
                tasksRows: tasks.map((task, index) => ({
                    task: task.description,
                    sphere: task.sphere.title,
                    mentor: task.creator.firstName + " " + task.creator.lastName,
                    start_date: setDate(task.startDate),
                    deadline: setDate(task.deadLine),
                    status: "finished",
                    id: task._id,
                    isSelected: false,
                    sort: false
                }))
            };
            ctx.status = 200;
            ctx.body = { tasks: data, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/tasks/sphere/:id/list', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const matchingKey = ctx.url.split('/')[2];
            const tasks = await getTasksById(id, matchingKey);
            ctx.status = 200;
            ctx.body = { tasks: tasks, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.post('/task', async (ctx, next) => {
        try {
            const newTask = await addTask(ctx.request.body);
            ctx.status = 201;
            ctx.message = 'task created';
            ctx.body = { data: newTask, message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.put('/task/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            await updateTask(id, ctx.request.body);
            ctx.status = 200;
            ctx.message = 'task edited';
            ctx.body = { message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    router.delete('/task/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            await deleteTask(id);
            ctx.status = 200;
            ctx.message = 'task deleted';
            ctx.body = { message: 'success' };
            await next();
        } catch (err) {
            throw err
        }
    });

    return router;
};

const setDate = date => {
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    if (month < 10){
        month = '0' + month
    }
    if (day < 10){
        day = '0' + day
    }
    return day+'.'+month+'.'+year
}

;

