const faker = require('faker');

const {
    getSphere,
    getSpheres,
    getSphereMentors,
    getSphereStudents,
    addSphere,
    updateSphere,
    deleteSphere,
    addMentor,
    removeMentor,
    addStudent,
    removeStudent,
    getAllMentors,
    getAllStudents,
    getSpheresTable

} = require('./../../../services/SphereService/index');

module.exports = function (router) {


    router.get('/spheres/list', async (ctx, next) => {
        try {
            const spheres = await getSpheres();
            ctx.status = 200;
            ctx.body = spheres;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/spheres/mentors/:id/list', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const mentors = await getSphereMentors(id);
            ctx.status = 200;
            ctx.body = mentors;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.post('/spheres/:id/mentors', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const data = ctx.request.body;
            const result = await addMentor(id, data);
            ctx.status = 200;
            ctx.body = result;
            await next();
        } catch (err) {
            throw err
        }
    });
    router.delete('/spheres/:id/mentors', async (ctx, next) => {
        try {
            const idSphere = ctx.params.id;
            const idMentor = ctx.request.body.mentor;
            const result = await removeMentor(idSphere, idMentor);
            ctx.status = 200;
            ctx.body = result;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.post('/spheres/:id/students', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const data = ctx.request.body;
            const result = await addStudent(id, data);
            ctx.status = 200;
            ctx.body = result;
            await next();
        } catch (err) {
            throw err
        }
    });
    router.delete('/spheres/:id/students', async (ctx, next) => {
        try {
            const idSphere = ctx.params.id;
            const idStudent = ctx.request.body.student;
            const result = await removeStudent(idSphere, idStudent);
            ctx.status = 200;
            ctx.body = result;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/spheres/students/:id/list', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const students = await getSphereStudents(id);
            ctx.status = 200;
            ctx.body = students;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/spheres/allSpheresTable/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const spheres = await getSpheresTable(id);
            const data = {
                tasksTitles: ["Name of the Sphere", "Number of members", "You role in sphere"],
                tasksRows: spheres.map((sphere) => {
                    console.log(sphere);
                    return {
                        name_of_the_sphere: sphere.title,
                        number_of_members: '24 members',
                        you_role_in_sphere: sphere.role,
                        id: sphere._id,
                        img: faker.image.avatar()
                    }})
            };
            ctx.status = 200;
            ctx.body = data;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/spheres/mentorstable', async (ctx, next) => {
        try {
            const mentors = await getAllMentors();
            const data = {
                tasksTitles: ["Mentor", "Spheres", "Number of students"],
                tasksRows: mentors.map((mentor) => {
                   return {
                    mentor: mentor.firstName + " " + mentor.lastName,
                    spheres: mentor.sphere,
                    number_of_students: '1 student',
                    id: mentor._id,
                    isSelected: false,
                    img: faker.image.avatar()
                }})
            };
            ctx.status = 200;
            ctx.body = data;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/spheres/studentstable', async (ctx, next) => {
        try {
            const students = await getAllStudents();
            const data = {
                tasksTitles: ["Student", "Spheres", "Number of tasks"],
                tasksRows: students.map((student) => ({
                    spheres: student.sphere,
                    number_of_tasks: "12 tasks",
                    student: student.firstName + " " + student.lastName,
                    id: student._id,
                    isSelected: false,
                    img: faker.image.avatar()
                }))
            };
            ctx.status = 200;
            ctx.body = data;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.post('/sphere', async (ctx, next) => {
        try {
           const sphere = await addSphere(ctx.request.body);
            ctx.status = 201;
            ctx.body = sphere;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.get('/sphere/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            const sphere = await getSphere(id);
            ctx.status = 200;
            ctx.body = sphere;
            await next();
        } catch (err) {
            throw err
        }
    });

    router.put('/sphere/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            await updateSphere(id, ctx.request.body);
            ctx.status = 200;
            ctx.body = 'sphere edited';
            await next();
        } catch (err) {
            throw err
        }
    });

    router.delete('/sphere/:id', async (ctx, next) => {
        try {
            const id = ctx.params.id;
            await deleteSphere(id);
            ctx.status = 200;
            ctx.body = 'sphere deleted';
            await next();
        } catch (err) {
            throw err
        }
    });
    return router;
};
