let compose = require('koa-compose');
let mount = require('koa-mount');
let Router = require('koa-router');

module.exports = function(prefix, api) {
  let router = new Router();
  if (prefix !== '/') {
    router.prefix(prefix);
  }

  router = api(router);
  return mount('/', compose([router.allowedMethods(), router.routes()]));
};
