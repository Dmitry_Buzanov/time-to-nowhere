module.exports = (url, id) => {
  let urlArr = url.split('/');
  urlArr.pop();
  urlArr.push(id);
  return urlArr.join('/');
};
