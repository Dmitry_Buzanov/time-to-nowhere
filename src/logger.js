let config = require('./../config');
let winston = require('winston');
winston.transports.LogstashUdp = require('winston-logstash-udp').LogstashUDP;

let container = new winston.Container();

// for log error of system
container.add('app', config.logs.app);
container.add('access', config.logs.access);

module.exports = {
  app: container.get('app'),
  access: container.get('access')
};
