const Koa = require('Koa');
const responseTime = require('koa-response-time');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const mount = require('koa-mount');
const logger = require('./middlewares/logger');
const config = require('./../config');
const app = new Koa();
const authorizationChecker = require('./middlewares/authChecker');
const authRoutes = require('./api/auth/routes');
const apiRoutes = require('./api/v2/index');
const session = require('koa-session');
const passport = require('./middlewares/passport');

app.keys = ['secret'];
app
  .use(responseTime())
  .use(cors())
  .use(bodyparser())
  .use(passport.initialize())
  /*.use(authorizationChecker)*/
  .use(mount('/auth', authRoutes))
  .use(mount('/api/v2', apiRoutes));

if (config.env !== 'testing') {
  app.use(logger());
}

module.exports = app;
