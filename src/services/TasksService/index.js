const Tasks = require('../../../db/models/Tasks');
const validateTask = require('../../api/validators/tasksValidator');
const { Types: { ObjectId } } = require('mongoose');
const mongoose = require('mongoose');

const lookupOptions = {
    spheres: {
        from: "spheres",
        localField: "sphere",
        foreignField: "_id",
        as: "sphere"
    },
    users: {
        creator: {
            from: "users",
            localField: "creator",
            foreignField: "_id",
            as: "creator"
        },
        performer: {
            from: "users",
            localField: "performer",
            foreignField: "_id",
            as: "performer"
        }
    }
}

const { spheres, users: { creator, performer } } = lookupOptions;


const getTasks = async () => {
    try {

        const tasks = await Tasks
            .aggregate()
            .lookup(spheres)
            .unwind("sphere")
            .lookup(performer)
            .unwind("creator")
            .lookup(creator)
            .unwind("performer")
        return tasks;
    } catch (err) {
        throw (err);
    }
};

const getTasksById = async (id, matchingKey) => {
    try {
        console.log("ID", id);
        const matchOptions = { [matchingKey]: ObjectId(id) };
        const task = await Tasks
            .aggregate()
            .match(matchOptions)
            .lookup(spheres)
            .unwind("sphere")
            .lookup(creator)
            .unwind("creator")
            .lookup(performer)
            .unwind("performer")
            .project({
                sphere:
                {
                    _id: 0,
                    __v: 0
                },
                creator: {
                    _id: 0,
                    __v: 0,
                    password:0
                },
                performer: {
                    __v: 0,
                    password: 0
                },
                __v: 0
            });
        return task;
    } catch (err) {
        throw (err);
    }
};

const addTask = async task => {
    try {
        const validation = await validateTask(task);
        if (!validation.error) {
            validation._id = new ObjectId();
            return await Tasks.create(validation);
        } else {
            throw validation.error
        }
    } catch (err) {
        throw (err);
    }
};

const updateTask = async (taskId, body) => {
    try {
        const validation = validateTask(body);
        if (!validation.error) {
            const task = await Tasks.findByIdAndUpdate(taskId, body, { new: true });
            return task;
        } else {
            throw validation.error
        }
    } catch (err) {
        throw err;
    }
};

const deleteTask = async taskId => {
    try {
        await Tasks.findByIdAndDelete(taskId);
    } catch (err) {
        throw err;
    }
};

module.exports = {
    getTasks,
    getTasksById,
    addTask,
    updateTask,
    deleteTask
};