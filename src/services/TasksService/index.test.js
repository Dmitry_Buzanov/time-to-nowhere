const mongoose = require('mongoose');
const config = require('../../../config');

const {
    getTask,
    getTasks,
    getTasksByUserId,
    getTasksBySphereId,
    addTask,
    updateTask,
    deleteTask
} = require('../TasksService/');


beforeAll(async () => {
    async function clearDB() {
        await Promise.all(
            Object.keys(mongoose.connection.collections).map(async (key) => {
                return await mongoose.connection.collections[key].remove({});
            }),
        );
    }

    if (mongoose.connection.readyState === 0) {
        await mongoose.connect(config.db.connection.url, { useNewUrlParser: true });
    }
    await clearDB();
});

afterAll(async () => {
    await mongoose.connection.close();
});

describe('READ tasks', () => {

    const id = new mongoose.Types.ObjectId();
    const userId = new mongoose.Types.ObjectId();
    const sphereId = new mongoose.Types.ObjectId();

    beforeAll(async () => {
        await addTask({
            _id: id.toHexString(),
            title: 'title',
            startDate: new Date(),
            deadLine: new Date(),
            sphere: sphereId.toHexString(),
            creator: userId.toHexString()
        });
    })

    test('read one task by id', async () => {
        const taskFromDb = await getTask(id);
        expect(taskFromDb.id).toEqual(id.toHexString());
    });

    test('read tasks by userId', async () => {
        const tasksByUserId = await getTasksByUserId(userId);
        expect(tasksByUserId.length > 0).toBeTruthy();
    });

    test('read one tasks by sphereId', async () => {
        const tasksBySphereId = await getTasksBySphereId(sphereId);
        expect(tasksBySphereId.length > 0).toBeTruthy();
    });

    test('read by non-existing id', async () => {
        const id = new mongoose.Types.ObjectId();
        const taskFromDb = await getTask(id);
        expect(taskFromDb).toBeNull();
    });

    test('read all tasks', async () => {
        const allTasks = await getTasks();
        expect(allTasks.length > 0).toBeTruthy();
    })
})
describe('CREATE task', () => {
    test('add task to db', async () => {

        const id = new mongoose.Types.ObjectId();
        const startDate = "2019-03-15";
        const endDate = "2019-03-16";

        const taskData = {
            _id: id.toHexString(),
            title: "some",
            startDate: new Date(startDate),
            deadLine: new Date(endDate)
        }

        const task = await addTask(taskData);
        expect(task.id).toEqual(id.toHexString());
        expect(task.title).toEqual(taskData.title);
        expect(task.startDate).toEqual(taskData.startDate);
        expect(task.deadLine).toEqual(taskData.deadLine);
    });
});

describe('UPDATE task', () => {
    const id = new mongoose.Types.ObjectId();

    beforeAll(async () => {
        await addTask({
            _id: id.toHexString(),
            title: 'title',
            startDate: new Date(),
            deadLine: new Date(),
        });
    })

    test('update task title', async () => {
        const newTitle = 'test';
        const updatedTask = await updateTask(id, { title: newTitle });
        expect(updatedTask).not.toBeNull;
        expect(updatedTask.title).toEqual(newTitle);
    })
});

describe('DELETE task', () => {
    const id = new mongoose.Types.ObjectId();

    beforeAll(async () => {
        await addTask({
            _id: id.toHexString(),
            title: 'title',
            startDate: new Date(),
            deadLine: new Date(),
        });
    })

    test('delete task by id', async () => {
        await deleteTask(id);
        const deletedTask = await getTask(id);
        expect(deletedTask).toBeNull();
    })
});
