const nodemailer = require('nodemailer');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require("fs"));
const {mail_user, mail_pass} = require('../../../config/development').links;
const handlebars = Promise.promisifyAll(require('handlebars'));
const {generateRefreshToken} = require("../../api/auth/controllers/generateToken");

const mailService = async(email,subject,htmlTemplatePath,url) =>{
    try{
        const transporter = nodemailer.createTransport(
            {
                service: "Mail.ru",
                auth: {
                    user: mail_user,
                    pass: mail_pass
                }
            }
        );
    
        const htmlTemplate = await fs.readFileAsync(htmlTemplatePath,"utf8");
        const template = await handlebars.compile(htmlTemplate);
        const token = await generateRefreshToken({email});
        const data = {
            url: `${url}/?token=${token}`
        };
        const html = await template(data);
        
        const message = {
             from: `EffectiveSoft<${mail_user}>`,
             to: email,
             subject: subject,
             html: html
        };
    
        const info = await transporter.sendMail(message)
        .catch((err)=>{//TODO: обертка для ошибок
            err.status = 400;
            err.message = "Can\'t send message";
            throw err;
        });
        transporter.close();
    }
    catch(err){
        throw err;
    }
}

module.exports = mailService;