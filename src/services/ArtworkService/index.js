const Artworks = require('./../../../db/models/Artworks');
const artworkValidation = require('../../api/validators/artworksValidator');

const getArtworks = async () => {
  try {
    const artworks = await Artworks.forge()
      .orderBy('id', 'ASC')
      .fetchAll();
    return artworks;
  } catch (err) {
    throw err;
  }
};

const getArtwork = async byId => {
  try {
    const id = parseInt(byId);
    const artwork = await Artworks.forge({ id: id }).fetch();
    return artwork;
  } catch (err) {
    throw err;
  }
};

const addArtwork = async body => {
  try {
    const result = await artworkValidation(body);
    if (!result.error) {
      const newArtwork = await Artworks.forge(body).save();
      return newArtwork.id;
    } else {
      throw result.error;
    }
  } catch (err) {
    throw err;
  }
};

const updateArtwork = async (byId, body) => {
  try {
    const id = parseInt(byId);
    const artworkById = await Artworks.forge({ id: id }).fetch();
    const result = await artworkValidation(body);
    if (!result.error) {
      const updatedArtwork = await artworkById.save(body);
      return updatedArtwork.id;
    } else {
      throw result.error;
    }
  } catch (err) {
    throw err;
  }
};

const deleteArtwork = async ctx => {
  try {
    await Artworks.forge({
      id: parseInt(ctx.params.id)
    }).destroy();
    return { id: ctx.params.id, deleted: true };
  } catch (err) {
    throw err;
  }
};

module.exports = { getArtworks, getArtwork, addArtwork, updateArtwork, deleteArtwork };
