const {addSphere, deleteSphere, getSphere, getSphereMentors, getSpheres, getSphereStudents, updateSphere} = require('./index');
const {addUser} = require('../UserService/index');
const mongoose = require('mongoose');
const config = require('../../../config');
const student_id = new mongoose.Types.ObjectId();
const mentor_id = new mongoose.Types.ObjectId();
const sphere_id = new mongoose.Types.ObjectId();

describe('Add spheres', () => {
    test('Add correct sphere', async () => {
        const newSphere = {
            title: 'testSphere1',
            category: "Python",
            image: '',
            mentors: [mentor_id],
            students: [{student_id: student_id, mentor_id: mentor_id}],
        };
        const data = await addSphere(newSphere);
        expect(data.error).toBeUndefined();
    });

    test('Add incorrect sphere', async () => {
        let allCorrect = true;
        const newSphere = {
            title: 12345,
            category: 'Python',
            image: '',
            mentors: mentor_id,
            students: [{student_id: student_id, mentor_id: mentor_id}],
        };
        try {
            await addSphere(newSphere);
        } catch (err) {
            allCorrect = false
        }
        expect(allCorrect).toBe(false);
    });
});


describe('Get spheres', () => {

    test('Get all spheres', async () => {
        let allCorrect = true;
        try {
            await getSpheres();
        } catch (err) {
            allCorrect = false
        }
        expect(allCorrect).toBe(true);
    });
    test('Get sphere by id', async () => {
        const result = await getSphere(sphere_id);
        expect(result._id).toEqual(sphere_id);
    });

    test('Get sphere mentors', async () => {
        await getSphereMentors(sphere_id);

    });
    test('Get sphere students', async () => {
        await getSphereStudents(sphere_id);

    });
});

describe('Update spheres', () => {
    test('Update sphere by id', async () => {
        const title = 'testTitle';
        const result = await updateSphere(sphere_id, {title: title});
        expect(result.title).toEqual(title);
    });
});

describe('Delete spheres', () => {
    test('Delete sphere by id', async () => {
        await deleteSphere(sphere_id);
        const result = getSphere(sphere_id);
        expect(result._id).toBeUndefined();
    });
});

beforeAll(async () => {
    async function clearDB() {
        await Promise.all(
            Object.keys(mongoose.connection.collections).map(async (key) => {
                return await mongoose.connection.collections[key].remove({});
            }),
        );
    }

    if (mongoose.connection.readyState === 0) {
        await mongoose.connect(config.db.connection.url, {useNewUrlParser: true});
    }
    await clearDB();
    await addSphere({
        _id: sphere_id,
        title: 'testSphere1',
        category: "Python",
        image: '',
        mentors: [mentor_id],
        students: [{student_id: student_id, mentor_id: mentor_id}],
    });
});

afterAll(() => {
    console.log('tests finished');
});



