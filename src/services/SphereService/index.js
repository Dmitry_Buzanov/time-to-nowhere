const Sphere = require("../../../db/models/Spheres/index");
const Users = require("../../../db/models/Users/index");
const spheresValidation = require('../../api/validators/sphereValidator');
const mongoose = require('mongoose');


const lookupOptions = {
    users: {
        mentors: {
            from: "users",
            localField: "mentors",
            foreignField: "_id",
            as: "mentors"
        },
        student_id: {
            from: "users",
            localField: "students.student_id",
            foreignField: "_id",
            as: "students"
        },

        mentor_id: {
            from: "users",
            localField: "students.mentor_id",
            foreignField: "_id",
            as: "students.mentor_id"
        }
    }
};


const {users: {mentors, student_id}} = lookupOptions;
const getSpheres = async () => {
    try {
        return await Sphere.aggregate()
            .lookup(mentors)
            .lookup(student_id)
    } catch (err) {
        throw err;
    }
};

const getSpheresTable = async (id) => {
    try {
        const spheres = await getSpheres();
        return spheres.map((sphere) => {
            sphere.mentors.map((mentor) => {
                if (mentor._id.toString() === id.toString()) {
                    sphere.role = "Mentor"
                }
                return mentor;
            });
            sphere.students.map((student) => {
                if (student._id.toString() === id.toString()) {
                    sphere.role = "Student"
                }
                return student;
            });
            if (!sphere.role) {
                sphere.role = "n/a"
            }
            return sphere;
        })
    } catch (err) {
        throw err;
    }
};

const getSphere = async Id => {
    try {
        return await Sphere.findOne({_id: Id});
    } catch (err) {
        throw err;
    }
};

const getSphereMentors = async id => {
    try {
        const sphere = await Sphere.findOne({_id: id});
        if (sphere.mentors) {
            return await Promise.all(sphere.mentors.map(async mentor => {
                return await Users.findOne({_id: mentor});
            }));
        }
        return "mentors is undefined";
    } catch (err) {
        throw err;
    }
};

const getAllMentors = async () => {
    try {
        const spheres = await getSpheres();
        const mentorsRes = [];
        await spheres.map((sphere) => {
            sphere.mentors.map((mentor) => {
                let isAdded = false;
                mentorsRes.map((resMentor) => {
                    if (resMentor._id === mentor._id) {
                        isAdded = true
                    }
                });
                if (!isAdded) {
                    mentor.sphere = sphere.title;
                    mentorsRes.push(mentor)
                }
            })
        });

        return mentorsRes
    } catch (err) {
        throw err;
    }
};

const getAllStudents = async () => {
    try {
        const spheres = await getSpheres();
        const studentRes = [];
        await spheres.map((sphere) => {
            console.log("sphere", sphere)
            sphere.students.map((student) => {
                let isAdded = false;
                studentRes.map((resStudent) => {
                    if (resStudent._id.toString() === student._id.toString()) {
                        isAdded = true;
                        resStudent.sphere = resStudent.sphere + ", " + sphere.title
                    }
                });
                if (!isAdded) {
                    student.sphere = sphere.title;
                    studentRes.push(student)
                }
            })
        });
        return studentRes
    } catch (err) {
        throw err;
    }
};

const getMentorStudents = async (id) => {
    try {
        const spheres = await getSpheres();
        let students = 0;
        await spheres.map((sphere) => {
            sphere.students.map((student) => {
                if (student.mentor_id.toString() === id.toString()) {
                    students = students + 1
                }
            })
        });
        return students
    } catch (err) {
        throw err;
    }
};

const getSphereStudents = async id => {
    try {
        const sphere = await Sphere.findOne({_id: id});
        if (sphere.students) {
            return await Promise.all(sphere.students.map(async student => {
                return await Users.findOne({_id: student.student_id});
            }))
        }
        return "students is undefined";
    } catch (err) {
        throw err;
    }
};

const addSphere = async body => {
    try {
        const result = await spheresValidation(body);
        if (!result.error) {
            addId(result)
            const sphere = new Sphere(result);
            await sphere.save();
            return sphere
        } else {
            throw result.error;
        }
    } catch (err) {
        throw err
    }
};


const updateSphere = async (Id, body) => {
    try {

        const result = await spheresValidation(body);
        if (!result.error) {

            await Sphere.findByIdAndUpdate(Id, body);
            return getSphere(Id)
        } else {
            throw result.error;
        }
    } catch (err) {
        throw err
    }
};
const deleteSphere = async id => {
    try {
        await Sphere.find({_id: id}).remove().exec();
    } catch (err) {
        throw err;
    }
};

const addId = async data => {
    if (!data._id) data._id = new mongoose.Types.ObjectId();
    return data
};

const addMentor = async (id, data) => {
    try {
        const sphere = await Sphere.findOne({_id: id});
        if (sphere.mentors.indexOf(data.mentor) !== -1) {
            return "This mentor already exists"
        }
        sphere.mentors.push(data.mentor);
        await Sphere.findByIdAndUpdate(id, sphere);
        return sphere
    } catch (err) {
        throw err;
    }
};

const removeMentor = async (idSphere, idMentor) => {
    try {
        const sphere = await Sphere.findOne({_id: idSphere});
        sphere.mentors = sphere.mentors.filter((item) => {
            return item.toString() !== idMentor
        });
        await Sphere.findByIdAndUpdate(idSphere, sphere);
        return sphere
    } catch (err) {
        throw err;
    }
};

const addStudent = async (id, data) => {
    try {
        const sphere = await Sphere.findOne({_id: id});

        sphere.students.push(data.student);
        await Sphere.findByIdAndUpdate(id, sphere);
        return sphere
    } catch (err) {
        throw err;
    }
};

const removeStudent = async (idSphere, idStudent) => {
    try {
        const sphere = await Sphere.findOne({_id: idSphere});
        sphere.students = sphere.students.filter((item) => {
            return item.student_id.toString() !== idStudent.toString()
        });
        await Sphere.findByIdAndUpdate(idSphere, sphere);
        return sphere
    } catch (err) {
        throw err;
    }
};

module.exports = {
    getSphere,
    getSpheres,
    getSphereStudents,
    getSphereMentors,
    addSphere,
    updateSphere,
    deleteSphere,
    addMentor,
    removeMentor,
    addStudent,
    removeStudent,
    getAllMentors,
    getAllStudents,
    getSpheresTable

};
