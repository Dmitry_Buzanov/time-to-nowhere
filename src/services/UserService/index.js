const User = require("../../../db/models/Users/index");
const Sphere = require("../../../db/models/Spheres/index");
const userValidation = require('../../api/validators/userValidator');
const mongoose = require('mongoose');

const lookupOptions = {
    users: {
        mentors: {
            from: "users",
            localField: "mentors",
            foreignField: "_id",
            as: "mentors"
        }
    }
};

const getUsers = async () => {
    try {
        /*return await Tasks.aggregate()
            .lookup(spheres)
            .unwind("sphere")
            .lookup(performer)
            .unwind("createdBy")
            .lookup(createdBy)
            .unwind("performer");*/

        return await User.find();
    } catch (err) {
        throw err;
    }
};

const getUser = async Id => {
    try {
        return await User.findOne({_id: Id});
    } catch (err) {
        throw err;
    }
};

const getUserStudents = async Id => {
    try {

    } catch (err) {
        throw err;
    }
};

const addUser = async body => {
    try {
        console.log("body", body);
        const result = await userValidation(body);
        if (!result.error) {
            await addId(result);
            const user = new User(result);
            await user.save();
            return user
        } else {
            throw result.error
        }
    } catch (err) {
        if (err.code === 11000) {
            err.message = 'Email is already exist';
            throw err
        } else {
            throw err
        }
    }
};

const updateUser = async (Id, body) => {
    try {
        const result = await userValidation(body);
        if (!result.error) {
             await User.findByIdAndUpdate(Id, body);
            return User.findOne({_id: Id});
        } else {
            throw result.error
        }
    } catch (err) {
        throw err
    }
};

const deleteUser = async id => {
    try {
        await User.find({_id: id}).remove().exec();
    } catch (err) {
        throw err;
    }
};

const addId = async data => {
    if (!data._id) data._id = new mongoose.Types.ObjectId();
    return data
};

module.exports = {getUsers, getUser, addUser, updateUser, deleteUser};
