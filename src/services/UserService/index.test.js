const { addUser, updateUser, deleteUser, getUsers, getUser } = require('./index');
const mongoose = require('mongoose');
const config = require('../../../config');
const faker = require('faker');
const id = new mongoose.Types.ObjectId();

const genders = ['female', 'male'];
const createNewUser = () => ({
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  avatar: '',
  position: faker.random.word(),
  birthDate: faker.date.past(),
  gender: faker.random.arrayElement(genders),
  role: faker.random.word()
});

describe('Add users', () => {
  test('Add correct user', async () => {
    const newUser = await createNewUser();
    console.log('newUser', newUser);
    const user = await addUser(newUser);
    expect(user.email).toEqual(newUser.email);
  });

  test('Add incorrect user', async () => {
    let allCorrect = true;
    const newUser = await createNewUser();
    newUser.gender = 123;
    try {
      console.log('avatar', faker.image.avatar());
      await addUser(newUser);
    } catch (err) {
      allCorrect = false;
    }
    expect(allCorrect).toBe(false);
  });
});

describe('Get users', () => {
  test('Get all users', async () => {
    let allCorrect = true;
    try {
      await getUsers();
    } catch (err) {
      allCorrect = false;
    }
    expect(allCorrect).toBe(true);
  });
  test('Get user by id', async () => {
    await getUser(id);
  });
});

describe('Update users', () => {
  test('Update user', async () => {
    const position = 'testpos';
    const result = await updateUser(id, { position: position });
    expect(result.position).toBe(position);
  });
});

describe('Delete users', () => {
  test('Delete user by id', async () => {
    await deleteUser(id);
    const result = getUser(id);
    expect(result._id).toBeUndefined();
  });
});

beforeAll(async () => {
  async function clearDB() {
    await Promise.all(
      Object.keys(mongoose.connection.collections).map(async key => {
        return await mongoose.connection.collections[key].remove({});
      })
    );
  }
  if (mongoose.connection.readyState === 0) {
    await mongoose.connect(
      config.db.connection.url,
      { useNewUrlParser: true }
    );
  }
  await clearDB();
  const newUser = createNewUser();
  newUser._id = id;
  await addUser(newUser);
});

afterAll(() => {
  console.log('tests finished');
});
