const config = require('./../config');
const connection = require('./app').listen(config.server.port);
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('debug', true);

mongoose.connection.on('connected', () => {
  console.log('Connection Established');
  if (connection) {
    console.log(`Server listening ${config.server.port}`);
  }
});

mongoose.connection.on('reconnected', () => {
  console.log('Connection Reestablished');
});

mongoose.connection.on('disconnected', () => {
  console.log('Connection Disconnected');
  if (connection) connection.close();
});

mongoose.connection.on('close', () => {
  console.log('Connection Closed');
  if (connection) connection.close();
});

mongoose.connection.on('error', error => {
  console.log('ERROR: ' + error);
  if (connection) connection.close();
});

const run = async () => {
  await mongoose.connect(
    'mongodb://localhost:27017/SkillSharing',
    {
      useNewUrlParser: true,
      autoReconnect: true,
      reconnectTries: 1000000,
      reconnectInterval: 1000
    }
  );
};

if (config.env !== 'testing') {
  run().catch(error => console.error(error));
}
