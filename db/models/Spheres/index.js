const mongoose = require('mongoose');
const { Schema: Types } = mongoose;

const Sphere = mongoose.Schema({
  _id: Types.ObjectId,
  title: { type: String, required: true },
  category: String,
  image: { data: Buffer, contentType: String },
  mentors: [Types.ObjectId],
  students: [{ student_id: { type: Types.ObjectId }, mentor_id: { type: Types.ObjectId } }]
});

module.exports = mongoose.model('Sphere', Sphere);
