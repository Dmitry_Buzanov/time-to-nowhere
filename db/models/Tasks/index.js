const mongoose = require('mongoose');
const {
  Schema,
  Schema: { Types }
} = mongoose;

const TaskScheme = new Schema({
  _id: Types.ObjectId,
  title: { type: String, required: true },
  description: String,
  sphere: { type: Types.ObjectId, ref: 'Sphere' },
  creator: { type: Types.ObjectId, ref: 'User' },
  performer: { type: Types.ObjectId, ref: 'User' },
  startDate: { type: Date, required: true },
  deadLine: { type: Date, required: true },
  completed: Boolean,
  subTasks: [{ text: { type: String, required: true }, completed: Boolean }]
});

module.exports = mongoose.model('Task', TaskScheme);
