const mongoose = require('mongoose');
const { Schema: Types } = mongoose;
const bcrypt = require('bcrypt');
const Promise = require('bluebird');
const SALT = 10;
const _bcrypt = Promise.promisifyAll(bcrypt);

const userSchema = mongoose.Schema({
  _id: Types.ObjectId,
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  avatar: { data: Buffer, contentType: String, required: false },
  position: { type: String, required: false },
  birthDate: { type: Date, required: false },
  gender: { type: String, required: false },
  role: { type: String, required: false },
  googleId: { type: String, required: false },
  facebookId: { type: String, required: false }
});

userSchema.pre('save', async function(next) {
  try {
    if (this.isModified('password')) {
      const salt = await _bcrypt.genSaltAsync(SALT);
      const hash = await _bcrypt.hashAsync(this.password, salt);
      this.password = hash;
    }
    next();
  } catch (err) {
    throw err;
  }
});

userSchema.methods.comparePasswords = async function(receivedPassword) {
  return _bcrypt.compareAsync(receivedPassword, this.password);
};

module.exports = mongoose.model('User', userSchema);
