const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoleScheme = new Schema({
  _id: Schema.Types.ObjectId,
  title: String
});

module.exports = mongoose.model('Role', RoleScheme);
