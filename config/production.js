module.exports = {
  env: 'production',
  server: {
    port: process.env.KOA_APP_PORT
  },
  db: {
    client: 'pg',
    connection: {
      ost: process.env.KOA_APP_DB_HOST,
      user: process.env.KOA_APP_DB_USER,
      password: process.env.KOA_APP_DB_PASSWORD,
      database: process.env.KOA_APP_DB_NAME,
      charset: process.env.KOA_APP_DB_CHARSET,
      multipleStatements: true,
      timezone: process.env.KOA_APP_DB_TIMEZONE
    },
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    }
  },
  session: {
    userBackTimeout: 3600 * 1000,
    endSessionTimeout: 3600 * 750
  },
  fileStorage: {},
  logs: {
    app: { console: { colorize: true, timestamp: true, level: 'debug' } },
    access: { console: { colorize: true, timestamp: true, level: 'debug' } }
  }
};
