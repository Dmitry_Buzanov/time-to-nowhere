const path = require('path');
const env = (process.env.NODE_ENV || 'production').trim();
require('./env');

module.exports = require(path.join(__dirname, env));
