const winston = require('winston');
const { combine, colorize, splat, simple } = winston.format;

module.exports = {
  env: 'development',
  server: {
    port: process.env.KOA_APP_PORT
  },
  secrets: {
    access_secret: process.env.KOA_APP_ACCESS_SECRET,
    refresh_secret: process.env.KOA_APP_REFRESH_SECRET,
    expires_in: process.env.KOA_APP_EXPRESS_IN
  },
  links: {
    mail_user: process.env.KOA_APP_MAIL_USER,
    mail_pass: process.env.KOA_APP_MAIL_PASS,
    password_url: process.env.KOA_APP_RESET_PASSWORD_URL,
    confirm_url: process.env.KOA_APP_CONFIRM_URL,
    password_change: process.env.KOA_APP_CHANGE_PASSWORD_URL,
    confirm_email: process.env.KOA_APP_CONFIRM_EMAIL_URL
  },
  db: {
    client: 'MongoDB',
    version: 'undef',
    debug: false,
    asyncStackTraces: false,
    connection: {
      url: process.env.KOA_APP_MONGO_URL
    },

    pool: {
      min: 1,
      max: 4,
      ping: function(conn, cb) {
        conn.query('SELECT 1', cb);
      }
    },
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    }
  },
  session: {
    userBackTimeout: 3600 * 1000
  },
  fileStorage: {},
  logs: {
    app: {
      format: combine(colorize(), splat(), simple()),
      transports: [
        new winston.transports.Console({ level: 'debug' }),
        new winston.transports.File({ filename: './logs/logfile.log' })
      ]
    },
    access: {
      format: combine(colorize(), splat(), simple()),
      transports: [
        new winston.transports.Console({ level: 'debug' }),
        new winston.transports.File({ filename: './logs/logfile.log' })
      ]
    }
  }
};
